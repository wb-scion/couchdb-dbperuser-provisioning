var constants = require('./constants');

function replicate_example_docs(source, target){

  return new Promise(function(resolve, reject) {

    if(source === target){
      return resolve(user);  //skip: we don't need ot copy to himself
    }

    request.get(
      source + '/_design/app/_view/byExampleProject',
      function(error, response, body) {
        if (error) {
          info && (error.info = info);
          reject(error);
          return;
        }
        
        var b = JSON.parse(body);
        var doc_ids = b.rows.map( function(r){ return r.id; } );

        var replicate_doc = {
          source,
          target,
          doc_ids
        };

        const logMessage = 'replicate example projects from ' + source + ' to ' + target;
        do_post(
          base_url + '/_replicate',
          replicate_doc, 
          function(resp) { 
            log(logMessage);
            resolve(user) 
          },
          reject,
          logMessage
        );
      }
    );
  });
}

if(require.module === module){
  replicate_example_docs(process.argv[1], process.argv[2]);
}

module.exports = replicate_example_docs;
